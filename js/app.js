// Select the elements
const clear= document.querySelector(".clear");
const dateElemet = document.getElementById("date");
const list = document.getElementById("list");
const input = document.getElementById("input");

// Classes names
const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const Line_Through = "lineThrough";


// variables

let List,id;

//get items from the localStorage
let data =localStorage.getItem('toDo');

// alert(data);
// check if data not empty
if(data){
  List= JSON.parse(data);
  id = List.Length;
  loadList(List);
}else{
  List = [];
  id = 0;
}

//load items to the user interface

function loadList(array){
  array.forEach(function(item){

    addToDo(item.name, item.id, item.done, item.trash);

  });
}

//clear the local storage

clear.addEventListener("click", function(event){

  localStorage.clear();
  location.reload();
});



//show Todays date
const options = { weekday: "long" , month: "short", day: "numeric"}
const today = new Date();
// document.getElementById("date").innerHTML = today;
dateElemet.innerHTML= today.toLocaleDateString('en-Us', options);


// add TODO function

function addToDo(toDo, id, done, trash){
if (trash) {return ;}
const DONE = done ? CHECK : UNCHECK;
const Line = done ? Line_Through : "";

const item = `
<li class="item">
  <i class="fa ${DONE} co" job='complete' id="${id}" ></i>
  <p class="text ${Line}" >${toDo}</p>
  <i class="fa fa-trash-o de" job='remove' id="${id}" ></i>
</li>
`;
const possition = "beforeend";

list.insertAdjacentHTML(possition, item);

}
// add an item to the list user by enter key
document.addEventListener('keyup', function (event){
  if (event.keyCode == 13) {
    const toDo = input.value;
    if (toDo){
      addToDo(toDo, id, false, false);
      List.push({
        name: toDo,
        id : id,
        done: false,
        trash: false
      });

      // add item to local storage
      localStorage.setItem("toDo", JSON.stringify(List));
      id++;
    }
    input.value = "";
  }
});


//complete TODO

function completeToDo(element){
    element.classList.toggle(CHECK);
    element.classList.toggle(UNCHECK);
    element.parentNode.querySelector('.text').classList.toggle(Line_Through);
    List[element.id].done = List[element.id].done ? false: true;
}


// remove TODO

function removeToDo(element){
    element.parentNode.parentNode.removeChild(element.parentNode );
    List[element.id].trash =true;

}

list.addEventListener('click',function(event){

  const element = event.target;
  const elementJob = element.attributes.job.value;

  if (elementJob == "complete"){ completeToDo(element);
  } else if(elementJob == "remove"){removeToDo(element)}

  // add item to local storage
  localStorage.setItem("toDo", JSON.stringify(List));
});
